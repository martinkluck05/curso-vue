Vue.component('password', {
    props: ['pass'],
    template: '<input type="password" :value="pass" @input="validatePassword($event.target.value)" ref="password">',
    methods: {
        validatePassword(password){
            if(this.notValid.includes(password)){
                this.$refs.password.value = password = '';
            }
            this.$emit('input', password);
        }
    },
    data(){
        return {
            notValid: ['abc', 'admin', '1234', 'root']
        }
    }
});

new Vue({
    el: 'main',
    data: {
        pass: 'manianaduermo'
    }
});