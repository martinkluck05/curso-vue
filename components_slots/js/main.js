Vue.component('alerta', {
    props: ['tipo','posicion'],
    template: '<section class="alerta" :class="[tipo, posicion]"><header class="alerta_header"><slot name="header">Hola</slot></header><div class="alerta_contenido"><slot>Lorem ipsum</slot></div><footer class="alerta_footer"><slot name="footer">Este es el footer</slot></footer></section>',
});

new Vue({
    el: 'main',
});