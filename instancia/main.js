new Vue({
	el:'main',
	data:{
		mensaje: 'Instancia 1',
	},
	beforeUpdate() {
		console.log('BeforeUpdate', this.mensaje);
	},
	updated() {
		console.log('updated', this.mensaje);
	},
	methods: {
		alReves(){
			this.mensaje = this.mensaje.split('').reverse().join('');
		}
	},
	computed: {
		mensajeMayusculas(){
			return this.mensaje.toUpperCase();
		}
	}
});