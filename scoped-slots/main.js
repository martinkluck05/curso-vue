Vue.component('mis-tareas', {
    props: ['listado'],
    template: '#mis-tareas-template'
});

new Vue({
    el: 'main',
    data: {
        tareas: [
            { titulo: 'Salir a correr'},
            { titulo: 'Salir a comer'},
            { titulo: 'Salir al cine'},
            { titulo: 'Salir a jugar'},
            { titulo: 'Salir a caminar'},
            { titulo: 'Salir a molestar'},
        ]
    }
});