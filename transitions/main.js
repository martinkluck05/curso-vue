new Vue({
	el: 'main',
	data: {
		mostrar: true,
		mensajes: {
			transicion: "Transiciones CSS con Vue.js",
			animacion: "Animaciones CSS con Vue.js",
			animacionCustom: 'Animaciones custom css con Vue.js',
			entreElementos: 'Transiciones entre elementos con Vue.js'
		}
	},
});